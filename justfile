serve:
    hugo server --buildDrafts & sleep 1 && xdg-open http://localhost:1313/ && wait

clean:
    rm -rf public .hugo_build.lock
